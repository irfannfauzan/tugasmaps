import 'package:delivert_system/UI/homepage.dart';
import 'package:delivert_system/UI/welcomescreen.dart';
import 'package:flutter/material.dart';
import 'package:here_sdk/core.dart';

void main() {
  SdkContext.init(IsolateOrigin.main);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/welcome': (context) => WelcomeScreen(),
        '/homepage': (context) => HomePageScreen(),
      },
      initialRoute: '/welcome',
      debugShowCheckedModeBanner: false,
      home: HomePageScreen(),
    );
  }
}
