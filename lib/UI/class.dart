// import 'dart:async';
// import 'dart:html';
// import 'package:here_sdk/core.dart';
// import 'package:location/location.dart ';
// class UserLocation {
//   final double latitude;
//   final double longitude;

//   UserLocation({this.latitude, this.longitude});
// }

// class LocationService {
//   StreamController<UserLocation> _locationStreamController =
//       StreamController<UserLocation>();
//   Stream<UserLocation> get locationStream => _locationStreamController.stream;

//   LocationService() {
//     Location location = Location();
//     location.requestPermission().then((permissionStatus) {
//       if (permissionStatus == PermissionStatus.granted) {
//         location.onLocationChanged.listen((LocationData) {
//           if (LocationData != null) {
//             _locationStreamController.add(UserLocation(
//                 latitude: LocationData.latitude,
//                 longitude: LocationData.longitude));
//           }
//         });
//       }
//     });
//   }
//   void dispose() => _locationStreamController.close();
// }
