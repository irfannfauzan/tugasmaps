import 'package:delivert_system/UI/detail.dart';
import 'package:delivert_system/UI/nearby.dart';
import 'package:delivert_system/data/data.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class AllHospital extends StatefulWidget {
  // const AllHospital({Key? key}) : super(key: key);

  @override
  _allHospitalState createState() => _allHospitalState();
}

class _allHospitalState extends State<AllHospital> {
  Position _currentUserPosition;
  double distanceImMeter = 0.0;
  Data data = Data();

  Future _getTheDistance() async {
    _currentUserPosition = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    print(_currentUserPosition.latitude);
    for (int i = 0; i < data.allstores.length; i++) {
      double storelat = data.allstores[i]['lat'];
      double storelng = data.allstores[i]['lng'];

      distanceImMeter = await Geolocator.distanceBetween(
        _currentUserPosition.latitude,
        _currentUserPosition.longitude,
        storelat,
        storelng,
      );
      var distance = distanceImMeter.round().toInt();

      data.allstores[i]['distance'] = (distance / 1000);
      setState(() {});
    }
  }

  @override
  void initState() {
    _getTheDistance();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue[900],
        title: Text("Seluruh Rumah Sakit"),
        automaticallyImplyLeading: false,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 3.0),
            child: IconButton(
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (_) => AlertDialog(
                            title: Text('Tampilkan Rumah Sakit Terdekat Saya?'),
                            content: Text('Apakah Anda Yakin?'),
                            actions: [
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => NearbyHospital()));
                                  },
                                  child: Text("Ya")),
                              ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("Tidak"))
                            ],
                          ));
                },
                icon: Icon(Icons.more_horiz)),
          )
        ],
      ),
      body: Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
          child: ListView.builder(
            itemCount: data.allstores.length,
            itemBuilder: (context, index) {
              if (data.allstores[index]['distance'] > 0) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => DetailsRumahSakit(
                            data: [data.allstores[index]],
                          ),
                        ));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Container(
                      margin: EdgeInsets.only(
                        left: 10,
                        right: 10,
                        bottom: 10,
                      ),
                      padding: EdgeInsets.only(
                        left: 10,
                        right: 10,
                        top: 26,
                      ),
                      height: 129,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white70,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.5),
                            spreadRadius: 0,
                            blurRadius: 7,
                            offset: Offset(0, 5), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                              bottom: 36,
                            ),
                            width: 63,
                            height: 63,
                            child: Icon(
                              Icons.location_pin,
                              size: 60,
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                              left: 7,
                              right: 7,
                            ),
                            child: Container(
                              width: 220,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    data.allstores[index]['name'],
                                  ),
                                  SizedBox(
                                    height: 7,
                                  ),
                                  Text(
                                    "Jalan Raya Jakarta Selatan Pasar Minggu Gandaria City",
                                  ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Spacer(),
                          Container(
                            width: 50,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(bottom: 11),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  child: Text(
                                    "${data.allstores[index]['distance'].round()} KM",
                                    style: TextStyle(fontSize: 13),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              } else {
                return Container();
              }
            },
          )),
    );
  }
}
