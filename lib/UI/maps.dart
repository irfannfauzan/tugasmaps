import 'dart:async';
import 'dart:typed_data';

import 'package:delivert_system/data/data.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:here_sdk/core.dart';
import 'package:here_sdk/mapview.dart';
import 'package:here_sdk/routing.dart';

class MapsUI extends StatefulWidget {
  List data = Data().allstores;
  MapsUI({this.data});

  @override
  _MapsUIState createState() => _MapsUIState();
}

class _MapsUIState extends State<MapsUI> {
  HereMapController _controller;
  MapPolyline _mapPolyline;
  Position _currentUserPosition;
  double distanceImMeter = 0.0;
  Data data = Data();

  Future _getTheDistance() async {
    _currentUserPosition = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    print(_currentUserPosition.latitude);
    for (int i = 0; i < data.allstores.length; i++) {
      double storelat = data.allstores[i]['lat'];
      double storelng = data.allstores[i]['lng'];

      distanceImMeter = await Geolocator.distanceBetween(
        _currentUserPosition.latitude,
        _currentUserPosition.longitude,
        storelat,
        storelng,
      );

      setState(() {});
    }
  }

  @override
  void initState() {
    _getTheDistance();
    super.initState();
  }

  @override
  void dispose() {
    _controller?.finalize();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.blue[900],
              leading: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back_outlined)),
              title: Text("Maps"),
            ),
            body: HereMap(
              onMapCreated: _onMapCreated,
            )));
  }

  Future<void> drawRedDot(HereMapController _heremapController, int drawOrder,
      GeoCoordinates geoCoordinates) async {
    ByteData fileData = await rootBundle.load('images/current.png');
    Uint8List pixelData = fileData.buffer.asUint8List();
    MapImage mapImage =
        MapImage.withPixelDataAndImageFormat(pixelData, ImageFormat.png);
    MapMarker mapMarker = MapMarker(geoCoordinates, mapImage);
    mapMarker.drawOrder = drawOrder;
    _heremapController.mapScene.addMapMarker(mapMarker);
  }

  Future<void> drawLocation(HereMapController _heremapController, int drawOrder,
      GeoCoordinates geoCoordinates) async {
    ByteData fileData = await rootBundle.load('images/desti.png');
    Uint8List pixelData = fileData.buffer.asUint8List();
    MapImage mapImage =
        MapImage.withPixelDataAndImageFormat(pixelData, ImageFormat.png);
    MapMarker mapMarker = MapMarker(geoCoordinates, mapImage);
    mapMarker.drawOrder = drawOrder;
    _heremapController.mapScene.addMapMarker(mapMarker);
  }

  Future<void> drawRoute(GeoCoordinates start, GeoCoordinates end,
      int drawOrder, HereMapController hereMapController) async {
    ByteData fileData = await rootBundle.load('images/current.png');
    Uint8List pixelData = fileData.buffer.asUint8List();
    MapImage mapImage =
        MapImage.withPixelDataAndImageFormat(pixelData, ImageFormat.png);
    MapMarker mapMarker = MapMarker(start, mapImage);
    mapMarker.drawOrder = drawOrder;
    hereMapController.mapScene.addMapMarker(mapMarker);

    RoutingEngine routingEngine = RoutingEngine();

    Waypoint startWaypoint = Waypoint.withDefaults(start);
    Waypoint endWaypoint = Waypoint.withDefaults(end);
    List<Waypoint> wayPoints = [startWaypoint, endWaypoint];

    routingEngine.calculateCarRoute(wayPoints, CarOptions.withDefaults(),
        (RoutingError, routes) {
      if (RoutingError == null) {
        var route = routes.first;
        GeoPolyline routeGeoPolyLine = GeoPolyline(route.polyline);
        double depth = 20;
        _mapPolyline = MapPolyline(routeGeoPolyLine, depth, Colors.blue);

        hereMapController.mapScene.addMapPolyline(_mapPolyline);
      }
    });
  }

  void _onMapCreated(HereMapController hereMapController) {
    _controller = hereMapController;
    hereMapController.mapScene.loadSceneForMapScheme(MapScheme.normalDay,
        (error) {
      if (error != null) {
        print('errorr' + error.toString());
        return;
      }
    });
    var LocationDestination =
        GeoCoordinates(widget.data[0]['lat'], widget.data[0]['lng']);
    drawRedDot(
        hereMapController,
        0,
        GeoCoordinates(
            _currentUserPosition.latitude, _currentUserPosition.longitude));
    drawLocation(hereMapController, 0,
        GeoCoordinates(widget.data[0]['lat'], widget.data[0]['lng']));
    drawRoute(
        GeoCoordinates(
            _currentUserPosition.latitude, _currentUserPosition.longitude),
        LocationDestination,
        1,
        hereMapController);

    double distanceToEarth = 8000;
    hereMapController.camera.lookAtPointWithDistance(
        GeoCoordinates(
            _currentUserPosition.latitude, _currentUserPosition.longitude),
        distanceToEarth);
  }
}
