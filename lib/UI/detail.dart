import 'package:delivert_system/UI/maps.dart';
import 'package:delivert_system/data/data.dart';
import 'package:flutter/material.dart';

class DetailsRumahSakit extends StatelessWidget {
  Data dataRes = Data();
  List data = Data().allstores;
  DetailsRumahSakit({this.data});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue[900],
          title: Text("Details Rumah Sakit"),
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back_outlined)),
        ),
        body: Padding(
            padding: const EdgeInsets.only(top: 0.0),
            child: ListView.builder(
              itemCount: 1,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    Container(
                      width: double.infinity,
                      height: 280,
                      decoration: BoxDecoration(),
                      child: Image.asset(
                        data[0]['image'],
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height / 1.7,
                      decoration: BoxDecoration(
                          color: Colors.white70,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.5),
                              spreadRadius: 1,
                              blurRadius: 7,
                              offset:
                                  Offset(0, 5), // changes position of shadow
                            ),
                          ],
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30),
                              topRight: Radius.circular(30))),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 25.0, top: 20, right: 25.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              decoration: BoxDecoration(),
                              child: Text(
                                data[0]['name'],
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 25),
                              ),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Row(
                              children: [
                                Icon(Icons.location_pin),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                    width: 250, child: Text(data[0]['alamat'])),
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Row(
                              children: [
                                Icon(Icons.call_end_outlined),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                    width: 200, child: Text(data[0]['telp'])),
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Row(
                              children: [
                                Icon(Icons.location_pin),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Text("Latitude : " +
                                            data[0]['lat'].toString()),
                                      ],
                                    ),
                                    SizedBox(height: 5),
                                    Row(
                                      children: [
                                        Text("Longitude : " +
                                            data[0]['lng'].toString()),
                                      ],
                                    ),
                                  ],
                                )),
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 25.0),
                                child: SizedBox(
                                    width: 180,
                                    child: ElevatedButton(
                                        style: ButtonStyle(
                                            shape: MaterialStateProperty.all(
                                                RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            13)))),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (_) => MapsUI(
                                                      data: [data[index]])));
                                        },
                                        child: Text("Petunjuk Arah"))),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                );
              },
            )),
      ),
    );
  }
}
