class Data {
  List allstores = [
    {
      "name": "Rumah Sakit Mayapada",
      "image": "images/rsmayapada.jpg",
      "alamat":
          "6, Jl. Lebak Bulus II No.86, RT.6/RW.4, Cilandak Bar., Kec. Cilandak, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12430",
      "telp": "(021) 2129217777",
      "lat": -6.2977915,
      "lng": 106.7857681,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Gandaria",
      "image": "images/rsgandaria.JPG",
      "alamat":
          "Jl. Gandaria Tengah II No.6, RT.6/RW.1, Kramat Pela, Kec. Kby. Baru, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12130",
      "telp": "(021) 7203311",
      "lat": -6.309355,
      "lng": 106.7351717,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Ibu dan Anak ASIH",
      "image": "images/ibuasih.JPG",
      "alamat":
          "Jl. Panglima Polim I No.34, RT.1/RW.3, Melawai, Kec. Kby. Baru, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12160",
      "telp": "(021) 2700610",
      "lat": -6.2478687,
      "lng": 106.8008104,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Jakarta",
      "image": "images/rsjakarta.JPG",
      "alamat":
          "Jl. Jend. Sudirman No.Kav 49, RT.5/RW.4, Karet Semanggi, Kecamatan Setiabudi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12930",
      "telp": "(021) 5732241",
      "lat": -6.2182371,
      "lng": 106.8161788,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Jakarta Medical Center (JMC)",
      "image": "images/rsmmc.JPG",
      "alamat":
          " Jl. Warung Buncit Raya No.15, RT.10/RW.5, Kalibata, Kec. Pancoran, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12740",
      "telp": "(021) 7980888",
      "lat": -6.2713908,
      "lng": 106.8302571,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Jakarta Marinir Cilandak",
      "image": "images/rsmarinir.JPG",
      "alamat":
          "Jl. Raya Cilandak Kko No.3, RW.5, Cilandak Tim., Kec. Ps. Minggu, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12560",
      "telp": "(021) 7805296",
      "lat": -6.3059247,
      "lng": 106.8092763,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Setia Mitra",
      "image": "images/rssm.JPG",
      "alamat":
          "Jl. RS. Fatmawati Raya No.80-82, RT.3/RW.10, Cilandak Bar., Kec. Cilandak, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12430",
      "telp": "(021) 7656000",
      "lat": -6.281336,
      "lng": 106.7961096,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Kebayoran Baru",
      "image": "images/rskebayoranlama.JPG",
      "alamat":
          "Jl. Abdul Majid Raya No.28, RT.2/RW.5, Cipete Utara, Kec. Kby. Baru, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12150",
      "telp": "(021) 22774429",
      "lat": -6.2655515,
      "lng": 106.7986147,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Pusat Pertamina",
      "image": "images/rspertamina.JPG",
      "alamat":
          "Jl. Kyai Maja No.43, RT.4/RW.8, Gunung, Kec. Kby. Baru, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12120",
      "telp": "(021) 7219000",
      "lat": -6.2410133,
      "lng": 106.7931627,
      "distance": 0,
    },
    {
      "name": "RSUD Pasar Minggu",
      "image": "images/rspasming.JPG",
      "alamat":
          "Jl. TB Simatupang No.1, RT.1/RW.5, Ragunan, Kec. Ps. Minggu, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12550",
      "telp": "(021) 29059999",
      "lat": -6.2940288,
      "lng": 106.8199372,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Kemang",
      "image": "images/rskemang.JPG",
      "alamat":
          "Jl. Ampera Raya No.34, Ragunan, Kec. Ps. Minggu, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12550",
      "telp": "(021) 27545454",
      "lat": -6.2860552,
      "lng": 106.8187165,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Umum Pusat Fatmawati",
      "image": "images/rsfatmawati.JPG",
      "alamat":
          "Jl. RS. Fatmawati Raya No.4, RT.4/RW.9, Cilandak Bar., Kec. Cilandak, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12430",
      "telp": "(021) 7501524",
      "lat": -6.344961,
      "lng": 106.7620537,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Asri Duren Tiga",
      "image": "images/rsdurentiga.JPG",
      "alamat":
          "Jl. Duren Tiga Raya No.48, RT.4/RW.1, Duren Tiga, Kec. Pancoran, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12760",
      "telp": "(021) -",
      "lat": -6.2539704,
      "lng": 106.8324833,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Bersalin Panti Nugraha",
      "image": "images/rsbersalin.JPG",
      "alamat":
          "Jl. Senayan No. 26 Rawa Barat Kebayoran Baru Jakarta Selatan DKI Jakarta, RT.3/RW.1, Melawai, Kec. Kby. Baru, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12160",
      "telp": "(021) 7221171",
      "lat": -6.3186304,
      "lng": 106.7270327,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Brawijaya Saharjo",
      "image": "images/rsbrawijaya.JPG",
      "alamat":
          "Jl. Dr. Saharjo No.199, RT.1/RW.1, Tebet Bar., Kec. Tebet, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12870",
      "telp": "(021) -",
      "lat": -6.2244964,
      "lng": 106.8477107,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Pondok Indah",
      "image": "images/rspik.JPG",
      "alamat":
          "Jl. Taman Duta I No.4, RT.4/RW.14, Pd. Pinang, Kec. Kby. Lama, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12310",
      "telp": "0815-1411-1070",
      "lat": -6.2834556,
      "lng": 106.7813396,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Tria Dipa",
      "image": "images/rstriadipa.JPG",
      "alamat":
          "Jl. Raya Pasar Minggu No.3A, RT.4/RW.1, Pancoran, Kec. Pancoran, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12780",
      "telp": "(021) 7993058",
      "lat": -6.2508683,
      "lng": 106.841929,
      "distance": 0,
    },
    {
      "name": "RSUD Tebet",
      "image": "images/rstebet.JPG",
      "alamat":
          "Jl. Prof. DR. Soepomo SH No.54, Tebet Bar., Kec. Tebet, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12810",
      "telp": "(021) 8314955",
      "lat": -6.2316972,
      "lng": 106.8466389,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit MMC",
      "image": "images/rsmmc2.JPG",
      "alamat":
          "Metropolitan Medical Centre Jl. H.R.Rasuna Said Kav.C-21 Karet Semanggi RT.11, RT.1/RW.7, Karet Kuningan, Kecamatan Setiabudi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12940",
      "telp": "(021) 5203435",
      "lat": -6.2200821,
      "lng": 106.8324797,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Siloam TB Simatupang",
      "image": "images/rssiloam.JPG",
      "alamat":
          "Jl. R.A. Kartini No.8, RW.4, Cilandak Bar., Kec. Cilandak, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12430",
      "telp": "(021) 29531900",
      "lat": -6.2921084,
      "lng": 106.7843444,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Siloam Mampang",
      "image": "images/rssiloammampang.JPG",
      "alamat":
          "Jl. Mampang Prpt. Raya No.16, RT.2/RW.5, Duren Tiga, Kec. Pancoran, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12760",
      "telp": "(021) 50102911",
      "lat": -6.2535813,
      "lng": 106.8277944,
      "distance": 0,
    },
    {
      "name": "RSUD Mampang Prapatan",
      "image": "images/rsmampang.JPG",
      "alamat":
          "6, Jl. Kapten Tendean No.9, RT.1/RW.5, Mampang Prpt., Kec. Mampang Prpt., Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12710",
      "telp": "(021) 7971115",
      "lat": -6.2405762,
      "lng": 106.8269058,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Aulia",
      "image": "images/rsaulia.JPG",
      "alamat":
          "11, Jl. Jeruk Raya No.15, RT.11/RW.1, Jagakarsa, Kec. Jagakarsa, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12620",
      "telp": "(021) 21798462",
      "lat": -6.3355731,
      "lng": 106.825761,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Yadika Kebayoran Lama",
      "image": "images/rsudkebayoranlama.JPG",
      "alamat":
          "Jl. Ciputat Raya No.5, RT.13/RW.1, Kby. Lama Sel., Kec. Kby. Lama, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12240",
      "telp": "(021) 7291074",
      "lat": -6.2551136,
      "lng": 106.7776557,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Simprug Pertamina",
      "image": "images/noimage.JPG",
      "alamat":
          "Jl. Sinabung II No.7, RT.7/RW.8, Grogol Sel., Kec. Kby. Lama, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12220",
      "telp": "(021) -",
      "lat": -6.2276077,
      "lng": 106.7892651,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Brawijaya Antasari",
      "image": "images/rsantasari.JPG",
      "alamat":
          "Jl. Taman Brawijaya No.1, RT.3/RW.3, Cipete Utara, Kec. Kby. Baru, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12730",
      "telp": "(021) 7211337",
      "lat": -6.2573227,
      "lng": 106.8074256,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Umum Petukangan",
      "image": "images/rspetukangan.JPG",
      "alamat":
          "Jl. Ciledug Raya No.8A, RT.1/RW.3, Petukangan Sel., Kec. Pesanggrahan, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12270",
      "telp": "(021) 7340906",
      "lat": -6.2368615,
      "lng": 106.7516853,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Kebayoran Lama",
      "image": "images/rskebayoranlamaa.JPG",
      "alamat":
          "Jl. Jatayu, RT.1/RW.12, Kby. Lama Sel., Kec. Kby. Lama, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12240",
      "telp": "(021) 27939067",
      "lat": -6.2488253,
      "lng": 106.7853631,
      "distance": 0,
    },
    {
      "name": "Rumah Sakit Umum Andhika",
      "image": "images/rsandika.JPG",
      "alamat":
          "Gudang Baru, Jakarta Sel, Jl. Wr. Sila No.8, RT.6/RW.4, Ciganjur, Kec. Jagakarsa, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12630",
      "telp": "(021) 78887087",
      "lat": -6.3439395,
      "lng": 106.8143708,
      "distance": 0,
    },
  ];
}
